#!/usr/bin/env sh
# Licensed to the Apache Software Foundation (ASF) under one or more
# contributor license agreements.  See the NOTICE file distributed with
# this work for additional information regarding copyright ownership.
# The ASF licenses this file to You under the Apache License, Version 2.0
# (the "License"); you may not use this file except in compliance with
# the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# The Katta command script, see man katta

KATTA_CONF_DIR=${KATTA_CONF_DIR:-/etc/katta}

if [ -f "${KATTA_CONF_DIR}/katta-env.sh" ]; then
  . "${KATTA_CONF_DIR}/katta-env.sh"
fi

. /usr/share/katta/bin/katta-config.sh

# some Java parameters
JAVA=`which java 2>/dev/null`
if [ ! "x$JAVA_HOME" = "x" ]; then
    JAVA=$JAVA_HOME/bin/java
fi
if test -z "$JAVA"; then
    echo "No java found in the PATH. Please set JAVA_HOME."
    exit 1
fi
JAVA_HEAP_MAX=-Xmx1000m 

# check envvars which might override default args
if [ "$KATTA_HEAPSIZE" != "" ]; then
  #echo "run with heapsize $KATTA_HEAPSIZE"
  JAVA_HEAP_MAX="-Xmx""$KATTA_HEAPSIZE""m"
  #echo $JAVA_HEAP_MAX
fi

# CLASSPATH initially contains $KATTA_CONF_DIR
CLASSPATH="${KATTA_CONF_DIR}":/usr/share/java/katta-core.jar

# add user-specified CLASSPATH last
if [ "$KATTA_CLASSPATH" != "" ]; then
  CLASSPATH=${CLASSPATH}:${KATTA_CLASSPATH}
fi

if [ "$KATTA_LOGFILE" = "" ]; then
  KATTA_LOGFILE='katta.log'
fi

CLASS='net.sf.katta.Katta'

if [ "x$KATTA_LOG_DIR" != "x" ]; then
    # a user command should not write a logfile
    KATTA_OPTS="$KATTA_OPTS -Dkatta.log.dir=$KATTA_LOG_DIR"
    KATTA_OPTS="$KATTA_OPTS -Dkatta.log.file=$KATTA_LOGFILE"
fi
KATTA_OPTS="$KATTA_OPTS -Dkatta.root.logger=${KATTA_ROOT_LOGGER:-INFO,console}"

# run it
exec "$JAVA" $JAVA_HEAP_MAX $KATTA_OPTS -classpath "$CLASSPATH" $CLASS "$@"
