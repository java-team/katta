=====
katta
=====

---------------------------------------------------------
scalable, distributed, indexed, data storage using hadoop
---------------------------------------------------------

:Author: Thomas Koch <thomas.koch@ymc.ch>
:Date: 2010-02-11

.. :Copyright: 

:Version: 0.1
:Manual section: 1
:Manual group: cluster computing

SYNOPSIS
========

Usage: katta [OPTIONS] COMMAND [COMMAND Specific options]

DESCRIPTION
===========

Katta is a script to communicate with a katta cluster. See
http://katta.sourceforge.net for more details about katta.

COMMANDS
========

listNodes 
    Prints out a list of all nodes.

listIndices 
    Prints out a list of all indexes.

showStructure 
    Prints out a hierarchical view of the Katta system.

check 
    Prints out useful deployment information.

version 
    Prints the katta version

addIndex <index name> <path to index> [<replication level>] 
    Deploys an index

removeIndex <index name> 
    Undeploys an index

redeployIndex <index name> 
    Redeploys an index

listErrors <index name> 
    Prints all errors of an index

search <index name> <query> 
    Searches in the specified index with the specified lucene query.

OPTIONS
=======

--config KATTA_CONF_DIR
    specifies an alternative config location instead of /etc/katta

--hosts HOSTSFILE
    specifies which nodefile inside KATTA_CONF_DIR to use.

FILES
=====

/etc/katta/*
    Katta configuration

ENVIRONMENT
===========

JAVA_HOME
    The java implementation to use.

KATTA_CLASSPATH
    Extra Java CLASSPATH entries.

KATTA_HEAPSIZE
    The maximum amount of heap to use, in MB. Default is 1000.

KATTA_OPTS
    Extra Java runtime options.

KATTA_CONF_DIR
    Alternate conf dir. Default is /etc/katta.

KATTA_ROOT_LOGGER
    The root appender. Default is INFO,console

EXAMPLES
========

BUGS
====

Katta has not yet been widely tested on debian and lacks a lot of
documentation.

SEE ALSO
========

hadoop(1), zookeeper
