#! /bin/sh
### BEGIN INIT INFO
# Provides:          katta-node
# Required-Start:    $remote_fs
# Required-Stop:     $remote_fs
# Should-Start:      zookeeper
# Should-Stop:       zookeeper
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: katta node
# Description:       A katta node supervises node local katta operations
#                    
### END INIT INFO

# Author: Thomas Koch <thomas.koch@ymc.ch>

# Do NOT "set -e"

NAME=katta-node
COMMAND=startNode
# PATH should only include /usr/* if it runs after the mountnfs.sh script
DESC="katta master"
PIDDIR=/var/run/katta
PIDFILE=$PIDDIR/$NAME.pid
SCRIPTNAME=/etc/init.d/$NAME
DAEMON_ERRORLOG=daemon.err
DAEMON_DEBUGLOG=daemon.debug
CLIENT_STDOUT=daemon.info
CLIENT_STDERR=daemon.err

# Exit if the package is not installed
[ -e "/usr/share/doc/katta-masterd" ] || exit 0

# Read configuration variable file if it is present
[ -r /etc/default/$NAME ] && . /etc/default/$NAME

PATH=/sbin:/usr/sbin:/bin:/usr/bin
USER=katta
GROUP=katta

EXIT_SUCCESS=0
EXIT_FAILURE=1
DAEMON_OPTS="--errlog=$DAEMON_ERRORLOG \
            --dbglog=$DAEMON_DEBUGLOG \
            --stdout=$CLIENT_STDOUT \
            --stderr=$CLIENT_STDERR \
            --name=$NAME \
            --noconfig \
            --pidfiles=$PIDDIR \
            --user=$USER.$GROUP "

# Load the VERBOSE setting and other rcS variables
. /lib/init/vars.sh

# Define LSB log_* functions.
# Depend on lsb-base (>= 3.0-6) to ensure that this file is present.
. /lib/lsb/init-functions

is_running()
{
    if [ -d $PIDDIR ]
    then
        if [ ! -w $PIDDIR ]
        then
          log_failure_msg "You must be root. See Debian bug #569987"
          exit 1
        fi
        # daemon eventually complains, when the pidfiles dir is not 
        # writable, which isn't relevant when only probing
        daemon $DAEMON_OPTS \
               --running #>/dev/null 2>&1
	    return "$?"
    else
	    return $EXIT_FAILURE
    fi
}

#
# Function that starts the daemon/service
#
do_start()
{
    if [ ! -d $PIDDIR ]
    then
      mkdir -p $PIDDIR
    fi
    chown $USER:$GROUP $PIDDIR

    LOG_DIR=/var/log/katta

    if [ ! -d  $LOG_DIR ]
    then
      mkdir -p $LOG_DIR
    fi
    chown $USER:$GROUP $LOG_DIR

	# Return
	#   0 if daemon has been started
	#   1 if daemon was already running
	#   2 if daemon could not be started
    is_running
	RETVAL="$?"
	[ "$RETVAL" = $EXIT_SUCCESS ] && return 1

	CLASSPATH=/etc/katta:/usr/share/java/katta-core.jar:$CLASSPATH
    CLASS=net.sf.katta.Katta

    KATTA_OPTS="$KATTA_OPTS -Dkatta.log.file=${KATTA_LOG_FILE:-${NAME}.log}"
    KATTA_OPTS="$KATTA_OPTS -Dkatta.log.dir=${KATTA_LOG_DIR:-$LOG_DIR}"
    KATTA_OPTS="$KATTA_OPTS -Dkatta.root.logger=${KATTA_ROOT_LOGGER:-INFO,DRFA}"

    daemon $DAEMON_OPTS \
           -- java $JAVA_OPTS $KATTA_OPTS -cp $CLASSPATH $CLASS $COMMAND $ARGS

    is_running
	RETVAL="$?"
	[ "$RETVAL" = $EXIT_SUCCESS ] && return 0
	[ "$RETVAL" = $EXIT_FAILURE ] && return 2
    return $RETVAL
}

#
# Function that stops the daemon/service
#
do_stop()
{
	# Return
	#   0 if daemon has been stopped
	#   1 if daemon was already stopped
	#   2 if daemon could not be stopped
	#   other if a failure occurred
    is_running
	RETVAL="$?"
	[ "$RETVAL" = $EXIT_FAILURE ] && return 1
	# Wait for children to finish too if this is a daemon that forks
	# and if the daemon is only ever run from this initscript.
	# If the above conditions are not satisfied then add some other code
	# that waits for the process to drop all resources that could be
	# needed by services started subsequently.  A last resort is to
	# sleep for some time.
    daemon $DAEMON_OPTS \
           --stop

    # wait for termination
    for i in 1 2 3 4 5 6 7 8 9
    do
        is_running
	RETVAL="$?"
	[ "$RETVAL" = $EXIT_SUCCESS ] && sleep 1 && continue
	[ "$RETVAL" = $EXIT_FAILURE ] && return 0
        return $RETVAL
    done
    return 2
}

case "$1" in
  start)
	[ "$VERBOSE" != no ] && log_daemon_msg "Starting $DESC"
	do_start
	case "$?" in
		0|1) [ "$VERBOSE" != no ] && log_end_msg 0 ;;
		2) [ "$VERBOSE" != no ] && log_end_msg 1 ;;
	esac
	;;
  stop)
	[ "$VERBOSE" != no ] && log_daemon_msg "Stopping $DESC"
	do_stop
	case "$?" in
		0|1) [ "$VERBOSE" != no ] && log_end_msg 0 ;;
		2) [ "$VERBOSE" != no ] && log_end_msg 1 ;;
	esac
	;;
  status)
    is_running
	RETVAL="$?"
    if [ "$RETVAL" = $EXIT_SUCCESS ]; then
        log_success_msg "$NAME is running"
        exit 0
    elif [ "$RETVAL" = $EXIT_FAILURE ]; then
        log_failure_msg "$NAME is not running"
        exit 1
    else
        log_failure_msg "something went very wrong"
        exit $RETVAL
    fi
       ;;
  restart|force-reload)
	#
	# If the "reload" option is implemented then remove the
	# 'force-reload' alias
	#
	log_daemon_msg "Restarting $DESC" "$NAME"
	do_stop
	case "$?" in
	  0)
		do_start
		case "$?" in
			0) log_end_msg 0 ;;
			1) log_end_msg 1 ;; # Old process is still running
			*) log_end_msg 1 ;; # Failed to start
		esac
		;;
	  1)
	    log_progress_msg "Daemon was not running. Starting it now"
		do_start
		case "$?" in
			0) log_end_msg 0 ;;
			1) log_end_msg 1 ;; # Old process is still running
			*) log_end_msg 1 ;; # Failed to start
		esac
		;;
	  *)
	  	# Failed to stop
		log_end_msg 1
		;;
	esac
	;;
  *)
	echo "Usage: $SCRIPTNAME {start|stop|status|restart|force-reload}" >&2
	exit 3
	;;
esac

:
